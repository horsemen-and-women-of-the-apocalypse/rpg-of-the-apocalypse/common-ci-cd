# Common CI-CD

Microservices CI/CD configuration and database setup for *RPG of the Apocalypse* project.

## Local installation

### Vanilla way

See `README.md` for each project :

- [Client](https://gitlab.com/horsemen-and-women-of-the-apocalypse/rpg-of-the-apocalypse/client)
- [User](https://gitlab.com/horsemen-and-women-of-the-apocalypse/rpg-of-the-apocalypse/user)
- [Game](https://gitlab.com/horsemen-and-women-of-the-apocalypse/rpg-of-the-apocalypse/game)
- [World](https://gitlab.com/horsemen-and-women-of-the-apocalypse/rpg-of-the-apocalypse/world)

For the database, as each service uses the same one (MongoDB) and the same version (`4.4`), you can try to setup locally with the direct install or Docker Compose.

### Docker + Docker Compose 🐳

It's more a proof of concept, unfortunately, but it works on our server — *trust us*. 

If you have any questions, feel free to ask `Chaussette Furtive#5200` on Discord. She is the person in charge of this *apocalyptic* configuration.

#### Default used ports

| Service | `pre-prod` | `prod` |
| --- | --- | --- |
| Client | `80` | `8000` |
| User | `8080` | `8090` |
| Game | `8081` | `8091` |
| World | `8082` | `8092` |

#### Requirements

- Docker (version: `20.10.13`)
- [Docker Compose](https://github.com/docker/compose/releases/tag/v2.4.1) (version: `2.4.1`)

#### Steps

##### Database and network

1. Clone this repository :

```bash
git clone https://gitlab.com/horsemen-and-women-of-the-apocalypse/rpg-of-the-apocalypse/common-ci-cd.git

[At 2022-04-24] Checkout to `2-add-cd` branch :

```sh
git checkout 2-add-cd
```

2. Go to `./app` directory, it is where all app-related files go.

3. Set up Docker Compose secrets :
    1. Create a new directory named `private` in `./app`
    2. In this directory, for root user and service users, create the following password text files (you can use `echo {password} > db_{user}_password.txt` ) : 
        - `db_root_password.txt`
        - `db_user_password.txt`
        - `db_game_password.txt`
        - `db_world_password.txt`

⚠️ Let an empty line at the end (`LF`), otherwise Docker Compose won't be able to read the secret properly.

4. Copy `.env.local` file and rename it to `.env`.

##### Services

1. For each service, clone their repository in the `./app` folder :

```bash
git clone https://gitlab.com/horsemen-and-women-of-the-apocalypse/rpg-of-the-apocalypse/{service_name}
```

2. Go to `2-add-cd` branch (as of 2022-04-24).

```sh
git checkout 2-add-cd
```

To get the most recent updates in code (and if you want to build the images yourself), you should merge this branch with `master` or `develop`.

3. (for API only) Create configuration files `app.production.ini`, `app.test.ini` (if you want to use the pre-production environment).

⚠️ Do not change the ports as the Dockerfile expose these ports in the image.

In `database/url` field (or quite differently for `world` project), the value should look like that : `mongodb://game:{password}@mongodb:27017/rpga-db`. The password must match with the one set in secret files.

4. Create a `.env` file in the root of each project :

```
# Docker Compose environment variables for production
COMPOSE_PROJECT_NAME=rpga
COMPOSE_IGNORE_ORPHANS=True
# Uncomment this line if you want to download specific image in GitLab Container Registry :
# TAG={tag}
```

`COMPOSE_PROJECT_NAME` must match with the one you used in the common `.env` file previously (it helps up for running multiple same containers in the same host).

5. If you want to build each image :

```bash
docker compose -f docker-compose.yml -f docker-compose.{env}.yml build
```

> `{env}` can take either `prod` (production), or either `pre-prod` (pre-production)

File tree of `./app` should look like that at the end :

```bash
app
├──db
│   ├───mongo-init.js
│   └───data # Volume created for storing database
├──client
│   ├───...
│   ├───.env
│   ├───docker-compose.yml
│   └───docker-compose.{pre-prod/prod}.yml
├──user/game/world
│   ├───config
│   │   ├───app.production.ini
│   │   └───(app.test.ini)
│   ├───...
│   ├───.env
│   ├───docker-compose.yml
│   └───docker-compose.{pre-prod/prod}.yml
├──private
│   ├───db_root_password.txt
│   ├───db_game_password.txt
│   ├───db_world_password.txt
│   └───db_user_password.txt
├──.env
├──docker-compose.yml
└──docker-compose.{pre-prod/prod}.yml
```

#### ▶️ Run 

1. In `./app` directory, run firstly Database `mongodb` container to initialize it and also the network between the containers :

```bash
docker compose -f docker-compose.yml -f docker-compose.{pre-prod/prod}.yml up -d
```

2. (optional) In each `./service` directory, you can change services mapped ports in the `docker-compose.{pre-prod/prod}.yml` file.

```bash
docker compose -f docker-compose.yml -f docker-compose.{pre-prod/prod}.yml up -d
```

3. If you have built/downloaded image for pre-production environment (tagged with `{COMMIT_SHA}-dev`), you can run tests (container must be down, as the secrets are only available during execution) :

```
docker compose -f docker-compose.yml -f docker-compose.{pre-prod/prod}.yml run --rm {client/world/game-service} npm run test
```

4. Voilà!
